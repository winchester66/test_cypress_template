export const ELEMENTS = {
    inpt_fisrt_name: '.first_name',
    inpt_last_name: '.last_name',
    inpt_business_name: '.business_name',
    inpt_email: '.email',
    inpt_result: '#number',
    lbl_demonstration: '.container > .mb-0',
    lbl_nmb_one: '#numb1',
    lbl_nmp_two: '#numb2',
    btn_submit: '#demo'
}