const demonstrationElements = require('./pages/demonstration/elements').ELEMENTS

Cypress.Commands.add('captcha', () => {
    cy.get(demonstrationElements.lbl_nmb_one).invoke('text').then((nmb1) => {
        cy.get(demonstrationElements.lbl_nmp_two).invoke('text').then((nmb2) => {

            cy.get(demonstrationElements.inpt_result)
                .type(parseFloat(nmb1) + parseFloat(nmb2))
        });
    });
})

