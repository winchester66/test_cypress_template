import '../../support/globalAfterEach';

const demonstrationElements = require('../../support/pages/demonstration/elements').ELEMENTS
const demonstrationMassa = Cypress.env('demonstration_massa');
describe('Testes na Tela de Demonstração', () => {
    beforeEach(() => {
        cy.visit('/demo')
    });

    it('Solicitação de demonstração', () => {
        cy.get(demonstrationElements.lbl_demonstration).should('be.visible')

        cy.get(demonstrationElements.inpt_fisrt_name).type(demonstrationMassa.first_name)
        cy.get(demonstrationElements.inpt_last_name).type(demonstrationMassa.last_name)
        cy.get(demonstrationElements.inpt_business_name).type(demonstrationMassa.business_name)
        cy.get(demonstrationElements.inpt_email).type(demonstrationMassa.email)

        cy.captcha()
       
        cy.get(demonstrationElements.btn_submit).click()

        cy.get('.completed').should('be.visible')
    });
});