const apiBaseUrl = Cypress.config('apiBaseUrl');
describe('Teste da API Star Wars', () => {
    it('Deve retornar os dados corretos de Luke Skywalker', () => {
        cy.getCharacterInfo(`${apiBaseUrl}/people/1`)
            .then((response) => {
                expect(response.status).to.eq(200); // Verifica se o status da resposta é 200 (OK)
                expect(response.body).to.have.property('name', 'Luke Skywalker'); // Verifica se o nome é "Luke Skywalker"
                expect(response.body).to.have.property('height', '172'); // Verifica se a altura é "172"
                // Verifica se outras propriedades estão presentes e têm os valores corretos
                expect(response.body).to.have.property('mass', '77');
                expect(response.body).to.have.property('hair_color', 'blond');
                expect(response.body).to.have.property('skin_color', 'fair');
                expect(response.body).to.have.property('eye_color', 'blue');
                expect(response.body).to.have.property('birth_year', '19BBY');
                expect(response.body).to.have.property('gender', 'male');
                // Verifica se o URL da API é o esperado
                expect(response.body).to.have.property('url', 'https://swapi.dev/api/people/1/');
                // Verifica se os URLs dos filmes, veículos e naves estelares são arrays não vazios
                expect(response.body.films).to.be.an('array').that.is.not.empty;
                expect(response.body.vehicles).to.be.an('array').that.is.not.empty;
                expect(response.body.starships).to.be.an('array').that.is.not.empty;
            });
    });

    it('Deve retornar status 404 para um recurso inexistente', () => {
        cy.request({
            method: 'GET',
            url: `${apiBaseUrl}/people/9999 `, // URL de um recurso inexistente
            failOnStatusCode: false // Permite que o teste continue mesmo se a requisição falhar
        }).then((response) => {
            expect(response.status).to.eq(404); // Verifica se o status da resposta é 404 (Not Found)
        });
    });
});
