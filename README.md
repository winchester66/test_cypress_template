# Template de Testes Automatizados com Cypress

Este é um projeto de testes automatizados desenvolvido em Cypress para ser referência de uma boa criação de estrutura de automação. Ele inclui configurações para gerar relatórios HTML automaticamente, integração com Actions do GitHub e utiliza o padrão Page Object para organizar os testes. Além de contar com testes de API. É utilizado de uma arquitetura simples, mas bem estruturada com os padrões de qualidade mais mundo dos testes.

## Pré-requisitos

- Node.js e npm instalados no seu sistema.
- Acesso a um repositório no GitHub com Actions habilitadas.

## Configurações

Este projeto utiliza Cypress com as seguintes configurações:

- `cypress.json`: Arquivo de configuração principal do Cypress, que inclui configurações como a URL base da aplicação, ambiente, configurações de relatório, entre outros.
- `.github/workflows/cypress.yml`: Configuração das GitHub Actions para executar os testes Cypress automaticamente quando ocorrerem pushs no repositório.

## Estrutura do Projeto

├── cypress

│ ├── fixtures/ # Arquivos de exemplo para carregar dados nos testes

│ ├── e2e/ # Arquivos de testes de api e user interface

│ ├── support/ # Scripts de suporte, como funções de ajuda

│ └── page-objects/ # Page Objects para organizar e reutilizar locators e métodos de ação

├── .github/

│ └── workflows/

│ └── cypress.yml # Configuração das GitHub Actions para testes automatizados

├── cypress.json # Arquivo de configuração principal do Cypress

├── .gitignore # Arquivo de configuração do Git para ignorar arquivos não necessários

└── package.json # Arquivo de configuração do Node.js com as dependências e scripts


## Instalação

1. Clone este repositório:
- git clone https://github.com/carlosmoreirabyx/test_cypress_template

2. Navegue até o diretório do projeto:
- cd test_cypress_template

3. Instale as dependências do projeto:
- npm install


## Executando os Testes

Para executar os testes Cypress localmente, você pode usar o comando:

- npx cypress run


Isso executará os testes em modo headless no navegador padrão definido nas configurações do Cypress.

## Visualizando Relatórios

Os relatórios HTML serão gerados automaticamente após a execução dos testes e estarão disponíveis na pasta `cypress/reports/html`. Eles podem ser visualizados abrindo o arquivo `index.html` em um navegador da web.

## Cypress Dashboard

Este projeto conta com a configuração de um Dashboard para melhor visualização dos testes que foram executados na esteira. Ao entrar em cada execução da esteira é possível ver ao fim da página um link para o Dashboard. 

## Autor

#### Carlos Moreira - Squad Zeus

Qualquer dúvida, entre em contato comigo via Linkedin:

- https://www.linkedin.com/in/carlos-moreira-887963122/

Ficarei feliz em ajudar.
